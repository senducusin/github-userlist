//
//  UserDetail.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import UIKit

class UserDetail: UITableViewController {
    // MARK: - Properties
    var githubUser: GithubUser
    private lazy var viewModel = DetailViewModel(githuUser: githubUser)
    private lazy var headerView = DetailHeaderView(frame: .init(x: 0, y: 0, width: view.frame.width, height: 280))
    
    // MARK: - Lifecycles
    init(githubUser: GithubUser){
        self.githubUser = githubUser
        super.init(nibName: nil, bundle: nil)
        
        DispatchQueue.main.async {
            self.setupUser()
        }
        loadUser()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupUI()
    }
    
    // MARK: - Helpers
    private func setupUI(){
        view.backgroundColor = .white
        title = githubUser.name
    }
    
    private func setupTableView(){
        tableView.rowHeight = 55
        tableView.tableHeaderView = headerView
        tableView.register(DetailCell.self, forCellReuseIdentifier: DetailCell.cellIdentifier)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 0)
    }
    
    private func setupUser(){
        viewModel = DetailViewModel(githuUser: githubUser)
        headerView.githubUser = githubUser
        title = githubUser.name
    }
    
    private func copyToClipboardValueOf(_ detail:Detail){
        UIPasteboard.general.string = detail.getStringValue()
        showQuickMessage("Copied to clipboard")
    }
    
    // MARK: - API
    private func loadUser(){
        guard let login = githubUser.login,
              let resource = GithubUser.user(withLogin: login) else {
            return
        }
        
        WebService.shared.load(resource: resource) { [weak self] result in
            
            switch result {
            
            case .success(let githubUser):
                DispatchQueue.main.async {
                    PersistenceService.shared.updateGithubUser(githubUser: githubUser) { [weak self] result in
                        switch result {
                        
                        case .success(let githubUser):
                            self?.githubUser = githubUser
                            self?.setupUser()
                            self?.tableView.reloadData()
                        case .failure(let error):
                            self?.showNotificationAlert(withTitle: "Oops!", message: error.localizedDescription)
                        }
                    }
                    self?.githubUser = githubUser
                    self?.tableView.reloadData()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.showNotificationAlert(withTitle: "Oops!", message: error.localizedDescription)
                }
            }
        }
    }
    
}

// MARK: - TableViewDataSource
extension UserDetail {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfDetails
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DetailCell.cellIdentifier, for: indexPath) as! DetailCell
        
        cell.detail = viewModel.detailAt(index: indexPath.row)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let detail = viewModel.detailAt(index: indexPath.row)
        copyToClipboardValueOf(detail)
    }
}
