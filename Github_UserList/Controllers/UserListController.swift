//
//  UserListController.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import UIKit

class UserListController: UITableViewController {
    // MARK: - Properties
    var viewModel = UserListViewModel()
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setupSearchController()
        setupUI()
        loadUsers()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.appStarted = true
    }
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var inSearchMode: Bool {
        return searchController.isActive && !searchController.searchBar.text!.isEmpty
    }
    
    // MARK: - Helpers
    private func setupUI(){
        view.backgroundColor = .white
        title = "Github Users"
    }
    
    private func loadUsers(){
        if viewModel.users.isEmpty {
            pullGithubUsers()
        }
    }
    
    private func setupTableView(){
        tableView.register(GithubUserCell.self, forCellReuseIdentifier: GithubUserCell.cellIdentifier)
        tableView.rowHeight = 70
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 0)
        tableView.keyboardDismissMode = .onDrag
    }
    
    private func setupSearchController(){
        searchController.searchResultsUpdater = self
        searchController.searchBar.showsCancelButton = false
        
        navigationItem.searchController = searchController
        
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = "Type a login name"
        
        definesPresentationContext = false
        
    }
    
    private func showDetailsOf(_ githubUser:GithubUser){
        let controller = UserDetail(githubUser: githubUser)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - API
    private func pullGithubUsers(){
        guard viewModel.usingWebService == false,
              let resource = ResponseGithubUser.all(pagination: viewModel.pagination) else {return}
        
        viewModel.usingWebService = true
        
        WebService.shared.load(resource: resource) { [weak self] result in
            switch result {
            
            case .success(let responseUsers):
                DispatchQueue.main.async {
                    PersistenceService.shared.persistResponseUsers(responseUsers) { error in
                        if let error = error {
                            self?.showNotificationAlert(withTitle: "Oops!", message: error.localizedDescription)
                            return
                        }
                        
                        self?.tableView.reloadData()
                    }
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.showNotificationAlert(withTitle: "Oops!", message: error.localizedDescription)
                }
            }
            
            self?.viewModel.usingWebService = false
        }
    }
}

// MARK: - TableView DataSource
extension UserListController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GithubUserCell.cellIdentifier, for: indexPath) as! GithubUserCell
        
        cell.githubUser = viewModel.userAt(index: indexPath.row)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let githubUser = viewModel.userAt(index: indexPath.row)
        
        showDetailsOf(githubUser)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - ScrollView Delegate
// Pagination Handling
extension UserListController {
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        viewModel.lastContentOffset = scrollView.contentOffset.y
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard viewModel.appStarted,
              !inSearchMode,
              viewModel.lastContentOffset < scrollView.contentOffset.y
        else {
            return
        }
        
        let position = scrollView.contentOffset.y
        if position > (tableView.contentSize.height - 100 - scrollView.frame.size.height) {
            pullGithubUsers()
        }
    }
}

// MARK: - SearchBar Delegate
extension UserListController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text?.lowercased() else {return}
        
        viewModel.searchText = searchText
        tableView.reloadData()
    }
    
}
