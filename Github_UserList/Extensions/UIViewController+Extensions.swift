//
//  UIViewController+Extensions.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import UIKit

extension UIViewController {
    func showQuickMessage(_ message:String){
        let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
        present(alert, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.45){
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    func showNotificationAlert(withTitle title: String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        present(alert, animated: true, completion: nil)
    }
}
