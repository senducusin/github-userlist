//
//  Detail.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import Foundation

struct Detail {
    let name: String
    let value: Any
    
    func getStringValue() ->String {
        return "\(value)"
    }
}
