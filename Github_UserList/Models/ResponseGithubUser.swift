//
//  ResponseGithubUser.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import Foundation

struct ResponseGithubUser: Codable {
    let login: String
    let id: Int
    let node_id: String
    let avatar_url: String
    let type: String
    let site_admin:Bool
}

extension ResponseGithubUser {
    static func all(pagination:Int = 0) -> Resource<[ResponseGithubUser]>? {
        guard let url = URL(string: "https://api.github.com/users?since=\(pagination)") else {return nil}
        
        return Resource(url: url)
    }
}
