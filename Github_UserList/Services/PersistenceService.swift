//
//  PersistenceService.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import Foundation
import RealmSwift

enum PersistenceError:Error{
    case loginDuplicate
    case noLoginValue
    case valueIsNil
}

class PersistenceService{
    static let shared = PersistenceService()
    let realm = try! Realm()
    
    private func saveResponseGithubUsers(_ responseGithubUsers:[ResponseGithubUser], completion:@escaping(Error?)->()){
        do{
            for responseGithubUser in responseGithubUsers {
                let githubUser = GithubUser(login: responseGithubUser.login,
                                            node_id: responseGithubUser.node_id,
                                            avatar_url: responseGithubUser.avatar_url,
                                            type: responseGithubUser.avatar_url,
                                            site_admin: responseGithubUser.site_admin,
                                            id: responseGithubUser.id)
                
                try realm.write{
                    realm.add(githubUser)
                }
            }
            completion(nil)
        }catch{
            completion(error)
        }
    }
    
    private func updateObject<T:Object>(object:T, withDictionary dictionary:[String:Any], completion:@escaping(Error?)->()){
        do{
            try realm.write{
                object.setValuesForKeys(dictionary)
            }
            completion(nil)
        }catch{
            completion(error)
        }
    }
    
    private func getObjectWithKey<T:Object>(object:T, key:String, value:Any?, completion:@escaping(Result<T,Error>)->()){
        guard let value = value else {
            completion(.failure(PersistenceError.valueIsNil))
            return
        }
        
        let query = "\(key) == %@"
        
        let objectResult = realm.objects(type(of: object)).filter(query,value)
        
        guard objectResult.count == 1,
              let object  = objectResult.first else {
            completion(.failure(PersistenceError.loginDuplicate))
            return
        }
        
        completion(.success(object))
    }
    
    
}

extension PersistenceService{
    public func getGithubUsers() -> Results<GithubUser>{
        return realm.objects(GithubUser.self).sorted(byKeyPath: "id", ascending: true)
    }
    
    public func persistResponseUsers(_ responseGithubUsers:[ResponseGithubUser], completion:@escaping(Error?)->()){
        saveResponseGithubUsers(responseGithubUsers, completion: completion)
    }
    
    public func getUsersWithFilter(keyword: String) -> Results<GithubUser> {
        return getGithubUsers().filter("login CONTAINS[cd] %@",keyword)
    }
    
    public func updateGithubUser(githubUser: GithubUser, completion: @escaping(Result<GithubUser,Error>)->()){
        guard let login = githubUser.login,
              let updatedKeys = githubUser.toDictionary() else {
            completion(.failure(PersistenceError.noLoginValue))
            return
        }
        
        getObjectWithKey(object: githubUser, key: "login", value: login) { [weak self] result in
            switch result {
            
            case .success(let userToUpdate):
                self?.updateObject(object: userToUpdate, withDictionary: updatedKeys) { error in
                    if let error = error {
                        completion(.failure(error))
                        return
                    }
                    
                    self?.getObjectWithKey(object: githubUser, key: "login", value: login) { result in
                        switch result {
                        
                        case .success(let updatedUser):
                            completion(.success(updatedUser))
                        case .failure(let error):
                            completion(.failure(error))
                        }
                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
