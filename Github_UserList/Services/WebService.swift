//
//  WebService.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import Foundation

enum NetworkError: Error {
    case domainError
    case urlError
    case decodeError
    case persistenceError
    case rateLimitError
}

extension NetworkError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        
        case .domainError:
            return NSLocalizedString("There was something wrong with the domain", comment: "Domain Error")
        case .urlError:
            return NSLocalizedString("URL format is invalid", comment: "URL Error")
        case .decodeError:
            return NSLocalizedString("Unable to decode data", comment: "Decode Error")
        case .persistenceError:
            return NSLocalizedString("Unable to save data", comment: "Persistence Error")
        case .rateLimitError:
            return NSLocalizedString("Rate limit exceeded", comment: "Rate Error")
        }
    }
}

enum HttpMethod: String {
    case post = "POST"
    case get = "GET"
}

struct Resource<T:Decodable>{
    var url: URL
    var httpMethod: HttpMethod = .get
    var httpBody: Data? = nil
}

class WebService{
    static let shared = WebService()
    
    func load<T>(resource: Resource<T>, completion:@escaping(Result<T,NetworkError>)->()){
        var request = URLRequest(url: resource.url)
        request.httpBody = resource.httpBody
        request.httpMethod = resource.httpMethod.rawValue
        request.addValue("json/application", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) {data, _, error in
            guard error == nil,
                  let data = data else {
                completion(.failure(.domainError))
                return
            }
            
            do {
                let result = try JSONDecoder().decode(T.self, from: data)
                completion(.success(result))
            }catch{
                
                do {
                    let errorResult = try JSONDecoder().decode(APIMessage.self, from: data)
                    if errorResult.message.hasPrefix("API rate limit exceeded") {
                        completion(.failure(.rateLimitError))
                    }
                    completion(.failure(.urlError))
                }catch{
                    completion(.failure(.decodeError))
                }
                
                
            }
            
        }.resume()
    }
}
