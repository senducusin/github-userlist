//
//  APIMessage.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import Foundation

struct APIMessage:Codable{
    let message: String
    let documentation_url: String
}
