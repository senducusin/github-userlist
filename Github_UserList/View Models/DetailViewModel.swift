//
//  DetailViewModel.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import Foundation

struct DetailViewModel{
    var githubUser:GithubUser
    var details = [Detail]()
    
    var numberOfDetails: Int {
        return details.count
    }
    
    func detailAt(index: Int) ->Detail {
        return details[index]
    }
    
    init(githuUser: GithubUser) {
        self.githubUser = githuUser
        
        guard let githubUserDictionary = githuUser.toDictionary() else {return}
        
        details = githubUserDictionary
            .filter{ (key, value) -> Bool in
                return !"\(value)".isEmpty &&
                    "\(value)" != "<null>"
            }
            .map({ Detail(name: detailName(name: $0.key), value: $0.value) })
            .sorted(by: {$0.name < $1.name})
    }
}

extension DetailViewModel {
    private func detailName(name:String)-> String {
        var newName = ""
        newName = name.replacingOccurrences(of: "_", with: " ")
        newName = newName.camelCaseToWords()
        
        if name == "login" {
            newName = "username"
        }
        
        return newName
    }
}
