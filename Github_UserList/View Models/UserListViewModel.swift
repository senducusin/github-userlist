//
//  UserListViewModel.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import Foundation
import RealmSwift

struct UserListViewModel {
    var appStarted = false
    
    var usingWebService = false
    
    var users = PersistenceService.shared.getGithubUsers().sorted(byKeyPath: "id", ascending: true)
    
    var numberOfRows: Int {
        return users.count
    }
    
    var pagination: Int {
        if let lastUser = users.last {
            return lastUser.id
        }
        
        return 0
    }
    
    var searchText: String? {
        didSet {
            guard let searchText = self.searchText else {return}
            
            if searchText.isEmpty {
                users = PersistenceService.shared.getGithubUsers()
            } else {
                users = PersistenceService.shared.getUsersWithFilter(keyword: searchText)
            }
        }
    }
    
    func userAt(index: Int) -> GithubUser {
        return users[index]
    }
    
    var lastContentOffset:CGFloat = 0
}

