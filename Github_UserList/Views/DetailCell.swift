//
//  DetailCell.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import UIKit

class DetailCell: UITableViewCell {
    // MARK: - Properties
    static let cellIdentifier = "DetailCell"
    
    var detail: Detail? {
        didSet{
            configure()
        }
    }
    
    private let keyLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 14)
        label.textColor = .black
        return label
    }()
    
    private let valueLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14)
        label.textColor = .lightGray
        return label
    }()
    
    
    // MARK: - Lifecycles
    override init(style: UITableViewCell.CellStyle, reuseIdentifier:String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Helpers
    private func setupUI(){
        let stack = UIStackView(arrangedSubviews: [keyLabel, valueLabel])
        stack.axis = .vertical
        addSubview(stack)
        
        stack.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 5, paddingLeft: 17, paddingBottom: 5, paddingRight: 17)
    }
    
    private func configure(){
        guard let detail = detail else {return}
        keyLabel.text = detail.name
        valueLabel.text = detail.getStringValue()
    }
}
