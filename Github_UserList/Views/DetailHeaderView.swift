//
//  DetailHeaderView.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import UIKit

class DetailHeaderView: UIView {
    // MARK: - Properties
    private let placeholderImage = UIImage(systemName: "person")
    
    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderColor = UIColor.darkGray.cgColor
        imageView.layer.borderWidth = 4.0
        imageView.tintColor = .darkGray
        imageView.backgroundColor = .lightGray
        imageView.image = placeholderImage
        
        let dimensions:CGFloat = 200
        
        imageView.setDimensions(height: dimensions, width: dimensions)
        imageView.layer.cornerRadius = dimensions/2
        return imageView
    }()
    
    private let followerLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 18)
        label.textColor = .darkGray
        label.textAlignment = .left
        label.text = "Follower: 0"
        return label
    }()
    
    private let followingLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 18)
        label.textColor = .darkGray
        label.textAlignment = .left
        label.text = "Following: 0"
        return label
    }()
    
    var githubUser: GithubUser? {
        didSet {
            configure()
        }
    }
    
    // MARK: - Lifecycles
    override init(frame: CGRect){
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helpers
    private func setupUI(){
        setupAvatar()
        setupFollowStack()
    }
    
    private func setupFollowStack(){
        let stack = UIStackView(arrangedSubviews: [followerLabel, followingLabel])
        stack.axis = .horizontal
        addSubview(stack)
        stack.anchor(top:avatarImageView.bottomAnchor, left:safeAreaLayoutGuide.leftAnchor, right: safeAreaLayoutGuide.rightAnchor, paddingTop: 16, paddingLeft: 16, paddingRight: 16)
    }
    
    private func setupAvatar(){
        addSubview(avatarImageView)
        avatarImageView.centerX(inView: self)
        avatarImageView.anchor(top:topAnchor, paddingTop: 26)
    }
    
    private func configure(){
        guard let githubUser = githubUser,
              let followers = githubUser.followers.value,
              let following = githubUser.following.value else {return}
        
        followingLabel.text = "Following: \(following)"
        followerLabel.text = "Followers: \(followers)"
        
        if let avatarUrl = githubUser.avatar_url{
            avatarImageView.kf.setImage(with: URL(string: avatarUrl))
        }
        
    }
}
