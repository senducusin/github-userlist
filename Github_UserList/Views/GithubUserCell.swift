//
//  GithubUserCell.swift
//  Github_UserList
//
//  Created by Jansen Ducusin on 4/20/21.
//

import UIKit
import Kingfisher

class GithubUserCell: UITableViewCell {
    //    MARK: - Properties
    static let cellIdentifier = "GithubUserCell"
    var githubUser: GithubUser? {
        didSet {
            configure()
        }
    }
    
    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .lightGray
        imageView.tintColor = .darkGray
        
        let dimension:CGFloat = 56
        imageView.setDimensions(height: dimension, width: dimension)
        imageView.layer.cornerRadius = dimension/2
        
        return imageView
    }()
    
    private let loginLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 16)
        label.textColor = .black
        return label
    }()
    
    //    MARK: - Lifecycles
    override init(style: UITableViewCell.CellStyle, reuseIdentifier:String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        accessoryType = .disclosureIndicator
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //    MARK: - Helpers
    private func setupUI(){
        setupAvatar()
        setupLoginLabel()
    }
    
    private func setupLoginLabel(){
        addSubview(loginLabel)
        loginLabel.centerY(inView: self, leftAnchor: avatarImageView.rightAnchor, paddingLeft: 10)
    }
    
    private func setupAvatar(){
        addSubview(avatarImageView)
        avatarImageView.centerY(inView: self, leftAnchor: leftAnchor, paddingLeft: 17)
    }
    
    private func configure(){
        guard let githubUser = githubUser,
              let username = githubUser.login else {return}
        
        loginLabel.text = "@\(username)"
        
        
        let placeholderImage = UIImage(systemName: "person")
        
        if let urlString = githubUser.avatar_url {
            avatarImageView.kf.setImage(with: URL(string: urlString),placeholder: placeholderImage)
        }else{
            avatarImageView.image = placeholderImage
        }
    }
}
