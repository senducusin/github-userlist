**Github Users List**

An app that displays Github users.

This app does not use storyboards except for the launch screen. 

---

## Features

- Display Github users  
- Display and cache avatar image urls  
- API used to fetch Github users [GET /users](https://api.github.com/users)   
- Display the error message informing "Rate limit exceeded" if the X-RateLimit-Remaining is 0.   
- Portrait and Landscape are both supported  

## Additional Features  

- Persist the displayed users   
- Search users from local database by login name (accessible by pulling the list while on the top)
- While on search mode, fetching of user is disabled  
- Pagination with lazy loading. Automatically gets the next batch of users when reaching the bottom of the list  
- Pagination is based on the last Github user ID.
- Added a detail view for all the users  
- In the detail view, tapping a detail on the list will copy the value to the clipboard.  

## Dependencies  
- Kingfisher: Image caching  
- RealmSwift: Data persistence  
---

## Demo

![](/Previews/Preview.gif)
